import 'package:flutter/widgets.dart';
import 'package:gabito/gabito.dart';
import 'package:gabito/src/handlers/middleware/middleware.dart';
import 'store.dart';
import 'view_model.dart';

typedef VMBuilder<T extends ViewModel> = T Function(
    EventHandler handler, Store store);
typedef InitVM<T extends ViewModel> = void Function(T vm);
typedef ViewModelChildBuilder<T> = Widget Function(BuildContext, T);
typedef FinalHandlersFactory = List<FinalHandler> Function();
typedef MiddlewareHandlersFactory = List<MiddlewareHandler> Function();

class ViewModelProvider<T extends ViewModel> extends StatefulWidget {
  final VMBuilder<T> vmBuilder;
  final BuildContext context;
  final ViewModelChildBuilder<T> builder;
  final InitVM<T> onInit;

  ViewModelProvider(
      {@required this.context,
      @required this.vmBuilder,
      @required this.builder,
      Key key,
      this.onInit})
      : assert(context != null),
        assert(vmBuilder != null),
        assert(builder != null),
        super(key: key);

  static T of<T extends ViewModel>(BuildContext context) =>
      (context.findAncestorStateOfType<_ViewModelProviderState>().viewModel
          as T);

  @override
  State<StatefulWidget> createState() {
    return _ViewModelProviderState<T>(builder, onInit);
  }
}

class _ViewModelProviderState<T extends ViewModel>
    extends State<ViewModelProvider> {
  T viewModel;
  ViewModelChildBuilder<T> builder;
  final InitVM<T> onInit;

  _ViewModelProviderState(this.builder, this.onInit);

  @override
  void didChangeDependencies() {
    if (viewModel != null) return;

    var middlewares =
        MiddlewaresFactoryProvider.of(context)?.middlewaresFactory() ??
            <MiddlewareHandler>[];
    var finalHandlers =
        FinalHandlersFactoryProvider.of(context)?.handlersFactory();

    assert(finalHandlers != null);

    var handler = MiddlewareHandler.buildPipe(middlewares
      ..add(MiddlewareHandler.fromFinalHandler(
          RouterEventHandler(finalHandlers))));

    viewModel = widget.vmBuilder(handler, StoreProvider.of(context).store);

    if (this.onInit != null) {
      this.onInit(viewModel);
    }

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    viewModel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (ctx) => this.builder(ctx, viewModel));
  }
}

class FinalHandlersFactoryProvider extends InheritedWidget {
  final FinalHandlersFactory handlersFactory;

  FinalHandlersFactoryProvider(
      {this.handlersFactory, @required WidgetBuilder builder})
      : assert(handlersFactory != null),
        assert(builder != null),
        super(
            child: Builder(
          builder: builder,
        ));

  static FinalHandlersFactoryProvider of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<FinalHandlersFactoryProvider>();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

class MiddlewaresFactoryProvider extends InheritedWidget {
  final MiddlewareHandlersFactory middlewaresFactory;

  MiddlewaresFactoryProvider(
      {@required this.middlewaresFactory, @required WidgetBuilder builder})
      : assert(middlewaresFactory != null),
        assert(builder != null),
        super(child: Builder(builder: builder));

  static MiddlewaresFactoryProvider of(BuildContext context) {
    return context
        .dependOnInheritedWidgetOfExactType<MiddlewaresFactoryProvider>();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}

class StoreProvider extends InheritedWidget {
  final Store store;

  StoreProvider({@required this.store, @required WidgetBuilder builder})
      : assert(store != null),
        assert(builder != null),
        super(child: Builder(builder: builder));

  static StoreProvider of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<StoreProvider>();
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return false;
  }
}

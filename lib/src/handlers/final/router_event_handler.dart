import 'dart:async';

import 'package:gabito/src/handlers/handler.dart';
import 'package:gabito/src/store.dart';

class RouterEventHandler extends FinalHandler {
  final List<FinalHandler> handlers;

  RouterEventHandler(this.handlers) : assert(handlers != null);

  @override
  Future<dynamic> handle(e, Store s, {StreamSink eventSink}) async {
    var handler =
        handlers.firstWhere((h) => h.canHandle(e), orElse: () => null);
    if (handler != null) {
      return handler.handle(e, s, eventSink:  eventSink);
    } else {
      throw NoEventHandlerFoundForEvent(e);
    }
  }
}

class NoEventHandlerFoundForEvent implements Exception {
  final event;

  NoEventHandlerFoundForEvent(this.event);

  @override
  String toString() => "No event handler found for event ${event.toString()}";
}

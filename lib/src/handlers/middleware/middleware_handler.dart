import 'dart:async';

import 'package:gabito/src/handlers/handler.dart';
import 'package:gabito/src/store.dart';

import 'delegate_middleware.dart';

abstract class MiddlewareHandler extends EventHandler {
  MiddlewareHandler next;

  MiddlewareHandler();

  FutureOr callNext(event, Store store, StreamSink eventSink) {
    if (next == null) {
      throw UnexpectedEndOfPipeException(event);
    }
    return next.handle(event, store, eventSink: eventSink);
  }

  factory MiddlewareHandler.fromFinalHandler(FinalHandler handler) {
    return DelegateMiddleware(handler);
  }

  factory MiddlewareHandler.buildPipe(List<MiddlewareHandler> middlewares) {
    if (middlewares.isEmpty) {
      throw BuildingPipeException(
          "Can't build a middleware pipe from an empty list of middlewares");
    }

    middlewares.fold<MiddlewareHandler>(middlewares.first, (c, n) {
      c.next = n;
      return n;
    });

    return middlewares.first;
  }
}

class UnexpectedEndOfPipeException implements Exception {
  final event;

  UnexpectedEndOfPipeException(this.event);
  @override
  String toString() {
    return "Unexpected end of pipe while processing: ${event.toString()}}";
  }
}

class BuildingPipeException implements Exception {
  final String message;

  BuildingPipeException(this.message);
  @override
  String toString() => this.message;
}

import 'dart:async';

import 'package:gabito/src/handlers/handler.dart';
import 'package:gabito/src/store.dart';

import 'middleware_handler.dart';

class DelegateMiddleware extends MiddlewareHandler {
  final FinalHandler handler;

  DelegateMiddleware(this.handler);

  @override
  FutureOr handle(event, Store store, {StreamSink eventSink}) {
    if (handler.canHandle(event)) {
      return handler.handle(event, store, eventSink: eventSink);
    }

    return callNext(event, store, eventSink);
  }
}

class Store {
  final _storage = Map<dynamic, dynamic>();

  V recover<V>(key) => (_storage[key] as V);

  void put(key, value) {
    _storage[key] = value;
  }

   void remove(key) {
    _storage.remove(key);
  }
}

abstract class StoreChanger {
  void change(Store store);
}

import 'dart:async';
import 'package:flutter/widgets.dart';
import 'package:gabito/src/handlers/handler.dart';
import 'store.dart';

abstract class ViewModel {
   EventHandler<dynamic, dynamic> handler;
  final Store store;

  ViewModel({
    @required this.handler,
    @required this.store,
  })  : assert(handler != null),
        assert(
          store != null,
        ) {
    eventsSC = StreamController.broadcast();
    responsesSC = StreamController<EventProcessingResult>.broadcast();

    initEventsProcessing();
  }

  void initEventsProcessing() {
    eventsSC.stream.listen((e) async {
      if (handler.canHandle(e)) {
        try {
          await processEvent(e);
        } catch (error) {
          onError(EventProcessingResult(e, error));
        }
      } else {
        throw NoSuitabledHandlerForEvent(e);
      }
    });
  }

  @mustCallSuper
  Future<void> processEvent(event) async {
    var response = await handler.handle(event, store, eventSink: eventsSC.sink);
    await onResponse(EventProcessingResult(event, response));
  }

  @mustCallSuper
  FutureOr<void> onResponse(EventProcessingResult responseData) {
    responsesSC.add(responseData);
  }

  @mustCallSuper
  FutureOr<void> onError(EventProcessingResult errorData) {
    responsesSC.addError(errorData);
  }

  StreamController eventsSC ;
  static final StreamController vmNotifications = StreamController.broadcast();
  StreamController<EventProcessingResult> responsesSC;

  Future newEvent(event) {
    final sc = StreamController();
    responses.where((r) => r.event == event).map((r) => r.response).take(1).listen((r) {
      sc.add(r);
      sc.close();
    }, onError: (e) {
      sc.addError((e as EventProcessingResult).response);
      sc.close();
    }, );

    eventsSC.add(event);
    return sc.stream.first;
  }

  Stream<EventProcessingResult> get responses => responsesSC.stream;
  Stream get events => eventsSC.stream;

  @mustCallSuper
  void dispose() {
    eventsSC.close();
    responsesSC.close();
  }
}

mixin HandlerResponsesUtilsMixin<E, S extends Store> on ViewModel {
  Stream<T> rot<T>() => responsesSC.stream
      .where((d) {
        return d.response is T;
      })
      .map((d) => d.response)
      .cast<T>();

  Stream<T> evot<T>() => eventsSC.stream.where((d) {
        return d is T;
      }).cast<T>();

  Stream<T> errot<T>() {
    return responsesSC.stream
        .transform(StreamTransformer.fromHandlers(
          handleData: (_, __) {},
          handleError: (e, t, s) {
            var event = (e as EventProcessingResult);
            if (event.response is T) s.add(event.response);
          },
        ))
        .cast<T>();
  }

  Stream<D> sfrom<D, E>() {
    var sc = StreamController<D>();
    rot<D>().listen((d) => sc.add(d), onDone: sc.close);
    errot<E>().listen((e) => sc.addError(e));
    return sc.stream;
  }
}

class NoSuitabledHandlerForEvent implements Exception {
  final event;

  NoSuitabledHandlerForEvent(this.event);
}

class EventProcessingResult {
  final dynamic event;
  final dynamic response;

  EventProcessingResult(this.event, this.response);
}

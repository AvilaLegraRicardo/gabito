import 'handlers/handlers.dart';
import 'viewmodel.dart';
import 'stream_filters.dart';

void main() {
  eventHandlerTest();
  routerHandlerTests();
  middlewareHandlerTests();
  viewModelTests();
  // streamFiltersTests();
}

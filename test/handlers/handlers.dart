import 'dart:async';
import 'package:matcher/matcher.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:gabito/gabito.dart';
import 'package:gabito/src/handlers/middleware/middleware.dart';

part 'event_handler.dart';
part 'router_handler.dart';
part 'middleware_handler.dart';

class Event {}

class EventA extends Event {}

class EventAA extends EventA {}

class EventAB extends EventA {}

class DefaultResponse {}

class EventAResponse {}

class EventAAResponse {} 

class EventABResponse {}

class EventAAHandler extends FinalHandler<EventAA, EventAAResponse> {
  @override
  FutureOr<EventAAResponse> handle(EventAA e, Store s, {StreamSink eventSink}) {
    return EventAAResponse();
  }
}

class AllEventsHandler extends FinalHandler<Event, dynamic> {
  @override
  FutureOr<dynamic> handle(e, Store s, {StreamSink eventSink}) {
    return DefaultResponse();
  }
}

class EventABHandler extends FinalHandler<EventAB, EventABResponse> {
  @override
  FutureOr<EventABResponse> handle(EventAB e, Store s,
      {StreamSink eventSink}) {
    return EventABResponse();
  }
}

class EventAHandler extends FinalHandler<EventA, EventAResponse> {
  @override
  FutureOr<EventAResponse> handle(EventA e, Store s, {StreamSink eventSink}) {
    return EventAResponse();
  }
}

class CustomEventHandler<E, R> extends FinalHandler<E, R> {
  final FutureOr<R> Function(E, Store, StreamSink) handleFunc;

  CustomEventHandler(this.handleFunc);
  @override
  FutureOr<R> handle(E e, Store s, {StreamSink eventSink}) {
    return handleFunc(e, s, eventSink);
  }
}

class PassMiddlewareHandler extends MiddlewareHandler {
  @override
  Future handle(event, Store store, {StreamSink eventSink}) {
    return callNext(event, store, eventSink);
  }
}

class CustomMiddlewareHandler extends MiddlewareHandler {
  final FutureOr Function(dynamic, Store, StreamSink, EventHandler next)
      handleFunc;

  CustomMiddlewareHandler(this.handleFunc);

  @override
  FutureOr handle(event, Store store, {StreamSink eventSink}) {
    return handleFunc(event, store, eventSink, next);
  }
}

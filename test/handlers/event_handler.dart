part of 'handlers.dart';

void eventHandlerTest() =>
    test('Un eventHandler puede manejar eventos dentro su misma jerarquía', () {
      final handler = AllEventsHandler();
      assert(handler.canHandle(Event()));
      assert(handler.canHandle(EventA()));
      assert(handler.canHandle(EventAA()));
    });

part of 'handlers.dart';

void routerHandlerTests() {
  group("RouterHandler", () {
    test(
        'Un routerHandler lanza una excepción de tipo NoEventHandlerFoundForEvent si no encuentra un handler apropiado para el evento',
        () {
      final routerHandler =
          RouterEventHandler([EventAAHandler(), EventABHandler()]);
      final event = EventA();
      expect(() => routerHandler.handle(event, Store()),
          throwsA(const TypeMatcher<NoEventHandlerFoundForEvent>()));
    });
    test(
        "Un routerHandler selecciona el primer handler que pueda manejar el evento",
        () async {
      final routerHandler = RouterEventHandler(
          [EventABHandler(), EventAHandler(), AllEventsHandler()]);
      final event = EventA();

      assert(await routerHandler
          .handle(event, Store())
          .then((r) => r is EventAResponse));
    });
  });
}
